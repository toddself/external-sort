const fs = require('fs')
const util = require('util')
const os = require('os')
const path = require('path')

const fsOpen = util.promisify(fs.open)
const fsRead = util.promisify(fs.read)
const fsClose = util.promisify(fs.close)

const cmp = (a, b) => parseInt(b, 10) - parseInt(a, 10)
const sep = '\n'
const enc = 'utf8'
const tmp = os.tmpdir()
const mode = 'r+'

function merge (a, b) {
  const out = []
  while (a.length + b.length > 0) {
    if (a.length === 0) {
      out.push(b.shift())
    } else if (b.length === 0) {
      out.push(a.shift())
    } else {
      if (cmp(a[0], b[0]) > 0) {
        out.push(b.shift())
      } else {
        out.push(a.shift())
      }
    }
  }
  return out
}

function mergeSort (arr) {
  if (arr.length < 2) return arr
  const center = Math.floor(arr.length / 2)
  return merge(mergeSort(arr.slice(0, center)), mergeSort(arr.slice(center)))
}

async function readLine (fd) {
  let i = ''
  let chr = ''
  do {
    const { bytesRead, buffer } = await fsRead(fd, Buffer.alloc(1), 0, 1, null)
    if (bytesRead === 0) return false
    chr = buffer.toString('utf8')
    i = `${i}${chr}`
  } while (chr !== sep)
  return parseInt(i, 10)
}

async function kMerge (count) {
  const fds = []
  let read = -1

  for (let i = 0; i <= count; i++) {
    fds.push(await fsOpen(path.join(tmp, `chunk-${i}`), mode))
  }

  do {
    const section = []
    for (let i = 0; i <= count; i++) {
      const line = await readLine(fds[i])
      if (line === false) read++
      else section.push(line)
    }
    const sorted = mergeSort(section)
    process.stdout.write(sorted.join(sep) + sep)
  } while (read !== count)

  for (let i = 0; i <= count; i++) {
    await fsClose(path.join(tmp, `chunk-${i}`))
  }
}

function externalSort (fn) {
  const s = fs.createReadStream(fn, { encoding: enc })
  let remain = ''
  let count = -1
  let wrote = -1
  let closed = false

  s.on('data', async (chunk) => {
    const lines = chunk.split('\n')
    if (remain) {
      lines[0] = parseInt(String(remain) + String(lines[0]), 10)
      remain = ''
    }
    if (lines[lines.length - 1] !== sep) remain = lines.pop()
    count++
    const sorted = mergeSort(lines)
    fs.writeFile(path.join(tmp, `chunk-${count}`), sorted.join(sep) + sep, enc, (err) => {
      if (err) throw err
      wrote++
      doMerge()
    })
  })

  s.on('end', async () => {
    closed = true
    doMerge()
  })

  async function doMerge () {
    if (count === wrote && closed) {
      try {
        await kMerge(count)
      } catch (err) {
        process.exit(1)
      }
    }
  }
}

module.exports = externalSort

if (!module.parent) {
  const fn = process.argv[2]
  if (!fn) throw new Error('You must supply a filename')
  externalSort(fn)
}
