#!/bin/sh
NODE=$(which node)
$NODE external-merge.js test/input.txt > test/out.txt
if diff --ignore-blank-lines test/expect.txt test/out.txt; then
  echo "Tests pass"
else
  echo "Tests fail"
fi
