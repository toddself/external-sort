# external sort

Sort a real large file that you can fit into memory. Or at least in a single chunk in a node fs read file stream.

## Usage

```sh
node external-merge.js [input file]
```

Will perform an external merge sort on the input file, outputting the sorted list to stdout.
